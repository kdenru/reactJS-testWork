import { takeEvery, put, call, fork, all } from 'redux-saga/effects';
import * as types from '../constants';
import * as api from '../../api';

function* getDepartments() {
  try {
    const response = yield call(api.getDepartments);
    yield put({ type: types.GET_DEPARTMENTS_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.GET_DEPARTMENTS_FAIL });
  }
}

function* getDepartment(action) {
  try {
    const response = yield call(api.getDepartment, action.payload);
    yield put({ type: types.GET_DEPARTMENT_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.GET_DEPARTMENT_FAIL });
  }
}

function* updateDepartment(action) {
  try {
    const response = yield call(api.updateDepartment, action.payload);
    yield put({ type: types.UPDATE_DEPARTMENT_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.UPDATE_DEPARTMENT_FAIL });
  }
}

function* getEmployeesByDepartment(action) {
  try {
    const response = yield call(api.getEmployeesByDepartment, action.payload);
    yield put({ type: types.GET_EMPLOYEES_BY_DEPARTMENT_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.GET_EMPLOYEES_BY_DEPARTMENT_FAIL });
  }
}

function* getEmployee(action) {
  try {
    const response = yield call(api.getEmployee, action.payload);
    yield put({ type: types.GET_EMPLOYEE_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.GET_EMPLOYEE_FAIL });
  }
}

function* updateEmployee(action) {
  try {
    const response = yield call(api.updateEmployee, action.payload);
    yield put({ type: types.UPDATE_EMPLOYEE_SUCCESS, payload: response.data });
  } catch (e) {
    yield put({ type: types.UPDATE_EMPLOYEE_FAIL });
  }
}

function* watchGetDepartment() {
  yield takeEvery(types.GET_DEPARTMENT, getDepartment);
}

function* watchGetEmployee() {
  yield takeEvery(types.GET_EMPLOYEE, getEmployee);
}

function* watchGetDepartments() {
  yield takeEvery(types.GET_DEPARTMENTS, getDepartments);
}

function* watchUpdateDepartment() {
  yield takeEvery(types.UPDATE_DEPARTMENT, updateDepartment);
}

function* watchUpdateEmployee() {
  yield takeEvery(types.UPDATE_EMPLOYEE, updateEmployee);
}

function* watchGetEmployeesByDepartment() {
  yield takeEvery(types.GET_EMPLOYEES_BY_DEPARTMENT, getEmployeesByDepartment);
}

export default function* root() {
  yield all([
    fork(watchGetEmployee),
    fork(watchGetDepartment),
    fork(watchGetDepartments),
    fork(watchUpdateEmployee),
    fork(watchUpdateDepartment),
    fork(watchGetEmployeesByDepartment),
  ]);
}
