import * as types from '../constants';

const initialState = {
  list: [],
  current: {
    first_name: '',
    last_name: '',
    departmentId: 0
  },
  selectedDepartment: 0
};

function getEmployeesByDepartment(id) {
  return { type: types.GET_EMPLOYEES_BY_DEPARTMENT, payload: id };
}

function selectDepartment(id) {
  return { type: types.SELECT_DEPARTMENT, payload: id };
}

function getEmployee(id) {
  return { type: types.GET_EMPLOYEE, payload: id };
}

function changeFirstName(name) {
  return { type: types.CHANGE_EMPLOYEE_FIRSTNAME, payload: name };
}

function changeLastName(name) {
  return { type: types.CHANGE_EMPLOYEE_LASTNAME, payload: name };
}

function changeDepartment(departmentId) {
  return { type: types.CHANGE_EMPLOYEE_DEPARTMENT, payload: departmentId };
}

function updateEmployee(employee) {
  return { type: types.UPDATE_EMPLOYEE, payload: employee };
}

function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case types.GET_EMPLOYEES_BY_DEPARTMENT_SUCCESS: {
      return {
        ...state,
        list: [...payload]
      };
    }
    case types.SELECT_DEPARTMENT: {
      return {
        ...state,
        selectedDepartment: payload
      };
    }
    case types.GET_EMPLOYEE_SUCCESS: {
      return {
        ...state,
        current: payload
      };
    }
    case types.CHANGE_EMPLOYEE_FIRSTNAME: {
      return {
        ...state,
        current: {
          ...state.current,
          first_name: payload
        }
      };
    }
    case types.CHANGE_EMPLOYEE_LASTNAME: {
      return {
        ...state,
        current: {
          ...state.current,
          last_name: payload
        }
      };
    }
    case types.CHANGE_EMPLOYEE_DEPARTMENT: {
      return {
        ...state,
        current: {
          ...state.current,
          departmentId: payload
        }
      };
    }
    default: return state;
  }
}

export default reducer;
export {
  getEmployee,
  changeLastName,
  updateEmployee,
  changeFirstName,
  selectDepartment,
  changeDepartment,
  getEmployeesByDepartment
};
