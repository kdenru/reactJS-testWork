import * as types from '../constants';

const initialState = {
  list: [],
  current: {
    department: {
      name: ''
    }
  }
};

function getDepartments() {
  return { type: types.GET_DEPARTMENTS };
}

function getDepartment(id) {
  return { type: types.GET_DEPARTMENT, payload: id };
}

function changeDepartmentName(name) {
  return { type: types.CHANGE_DEPARTMENT_NAME, payload: name };
}

function updateDepartment(department) {
  return { type: types.UPDATE_DEPARTMENT, payload: department };
}

function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case types.GET_DEPARTMENTS_SUCCESS: {
      return {
        ...state,
        list: payload
      };
    }
    case types.GET_DEPARTMENT_SUCCESS: {
      return {
        ...state,
        current: {
          ...state.current,
          department: payload
        }
      };
    }
    case types.CHANGE_DEPARTMENT_NAME: {
      return {
        ...state,
        current: {
          ...state.current,
          department: {
            ...state.current.department,
            name: payload
          }
        }
      };
    }
    default: return state;
  }
}

export default reducer;
export { getDepartments, getDepartment, changeDepartmentName, updateDepartment };

