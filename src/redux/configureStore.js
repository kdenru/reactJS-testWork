import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import 'babel-polyfill';
import departments from './modules/departments';
import employees from './modules/employees';
import rootSaga from './sagas/index';

const reducer = combineReducers({ departments, employees });
const sagaMiddleware = createSagaMiddleware();

function configureStore() {
  return {
    ...createStore(
      reducer,
      compose(
        applyMiddleware(sagaMiddleware),
        window.devToolsExtension())
      ),
    runSaga: sagaMiddleware.run(rootSaga)
  };
}

export default configureStore;
