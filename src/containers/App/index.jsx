import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import block from 'bem-cn';

import Sidebar from '../../components/Sidebar';
import './style.scss';

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  componentDidMount() {
    return this;
  }

  render() {
    const b = block('app');
    return (
      <div className={b()}>
        <div className="container">
          <Sidebar />
          <Col md={6}>
            { this.props.children }
          </Col>
        </div>
      </div>
    );
  }
}

export default App;
