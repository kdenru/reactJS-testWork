import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormGroup, FormControl, ControlLabel, Button, InputGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import block from 'bem-cn';

import * as actions from '../../../redux/modules/departments';

function mapStateToProps(state) {
  return {
    department: state.departments.current.department,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, actions), dispatch);
}


class DepartmentsDetails extends Component {
  static propTypes = {
    getDepartment: PropTypes.func.isRequired,
    updateDepartment: PropTypes.func.isRequired,
    changeDepartmentName: PropTypes.func.isRequired,
    params: PropTypes.objectOf(PropTypes.any).isRequired,
    department: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  constructor(props) {
    super(props);
    this.changeDepartmentName = this.changeDepartmentName.bind(this);
  }

  componentDidMount() {
    const { getDepartment, params: { id } } = this.props;
    getDepartment(id);
  }

  changeDepartmentName(e) {
    const { changeDepartmentName } = this.props;
    changeDepartmentName(e.target.value);
  }

  render() {
    const b = block('department');
    const { department, updateDepartment } = this.props;

    return (
      <div className={b()}>
        <h4>Edit department</h4>
        <FormGroup>
          <ControlLabel>Department name:</ControlLabel>
          <InputGroup>
            <FormControl
              value={department.name}
              onChange={this.changeDepartmentName}
              placeholder="Enter department name"
              type="text"
            />
            <InputGroup.Button>
              <Button onClick={() => updateDepartment(department)}>Save</Button>
            </InputGroup.Button>
          </InputGroup>
        </FormGroup>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentsDetails);
