import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import block from 'bem-cn';

import * as actions from '../../redux/modules/departments';

function mapStateToProps(state) {
  return {
    departments: state.departments.list
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, actions), dispatch);
}


class Departments extends Component {
  static propTypes = {
    getDepartments: PropTypes.func.isRequired,
    departments: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  componentDidMount() {
    const { getDepartments } = this.props;
    getDepartments();
  }

  render() {
    const b = block('departments');
    const { departments } = this.props;

    return (
      <div className={b()}>
        <h4>Departments:</h4>
        <ul>
          { departments.map(department =>
            <li key={shortid.generate()}>
              <Link to={`/departments/${department.id}`}>
                { department.name }
              </Link>
            </li>
          )}
        </ul>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Departments);
