import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Select from 'react-select';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import block from 'bem-cn';

import 'react-select/dist/react-select.css';

import * as employeeActions from '../../../redux/modules/employees';
import * as departmentActions from '../../../redux/modules/departments';


function mapStateToProps(state) {
  return {
    employee: state.employees.current,
    departments: state.departments.list
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, employeeActions, departmentActions), dispatch);
}


class EmployeeDetails extends Component {
  static propTypes = {
    getEmployee: PropTypes.func.isRequired,
    updateEmployee: PropTypes.func.isRequired,
    getDepartments: PropTypes.func.isRequired,
    changeLastName: PropTypes.func.isRequired,
    changeFirstName: PropTypes.func.isRequired,
    changeDepartment: PropTypes.func.isRequired,
    params: PropTypes.objectOf(PropTypes.any).isRequired,
    employee: PropTypes.objectOf(PropTypes.any).isRequired,
    departments: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  constructor(props) {
    super(props);
    this.changeLastName = this.changeLastName.bind(this);
    this.changeFirstName = this.changeFirstName.bind(this);
  }

  componentDidMount() {
    const { getEmployee, params: { id }, getDepartments } = this.props;
    getEmployee(id);
    getDepartments();
  }

  changeFirstName(e) {
    const { changeFirstName } = this.props;
    changeFirstName(e.target.value);
  }

  changeLastName(e) {
    const { changeLastName } = this.props;
    changeLastName(e.target.value);
  }

  render() {
    const b = block('employee');
    const { employee, departments, changeDepartment, updateEmployee } = this.props;

    const options = departments.map(department =>
      ({ value: department.id, label: department.name })
    );

    return (
      <div className={b()}>
        <h4>Edit employee</h4>
        <FormGroup>
          <ControlLabel>First name:</ControlLabel>
          <FormControl
            value={employee.first_name}
            onChange={this.changeFirstName}
            placeholder="Enter first name"
            type="text"
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Last name:</ControlLabel>
          <FormControl
            value={employee.last_name}
            onChange={this.changeLastName}
            placeholder="Enter last name"
            type="text"
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Department:</ControlLabel>
          <Select
            options={options}
            onChange={option => changeDepartment(option.value)}
            value={employee.departmentId}
            placeholder="Please select department"
            clearable={false}
          />
        </FormGroup>
        <FormGroup>
          <Button onClick={() => updateEmployee(employee)}>Save</Button>
        </FormGroup>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeDetails);
