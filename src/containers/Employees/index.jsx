import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Select from 'react-select';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import 'react-select/dist/react-select.css';

import block from 'bem-cn';

import * as employeesActions from '../../redux/modules/employees';
import * as departmentActions from '../../redux/modules/departments';

function mapStateToProps(state) {
  return {
    employees: state.employees.list,
    departments: state.departments.list,
    selectedDepartment: state.employees.selectedDepartment
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, employeesActions, departmentActions), dispatch);
}


class Employees extends Component {
  static propTypes = {
    departments: PropTypes.arrayOf(PropTypes.object).isRequired,
    employees: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedDepartment: PropTypes.number.isRequired,
    getEmployeesByDepartment: PropTypes.func.isRequired,
    selectDepartment: PropTypes.func.isRequired,
    getDepartments: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.selectDepartment = this.selectDepartment.bind(this);
  }

  componentDidMount() {
    const { getDepartments } = this.props;
    getDepartments();
  }

  selectDepartment(option) {
    const { getEmployeesByDepartment, selectDepartment } = this.props;
    getEmployeesByDepartment(option.value);
    selectDepartment(option.value);
  }

  render() {
    const b = block('employees');
    const { employees, departments, selectedDepartment } = this.props;
    const options = departments.map(department =>
      ({ value: department.id, label: department.name })
    );
    return (
      <div className={b()}>
        <Select
          options={options}
          value={selectedDepartment}
          onChange={this.selectDepartment}
          placeholder="Please select department"
          clearable={false}
        />
        { employees.length ?
          <div className={b('list')}>
            <h4>Employees:</h4>
            <ul>
              { employees.map(employee =>
                <li key={shortid.generate()}>
                  <Link to={`/employees/${employee.id}`}>
                    {`${employee.first_name} ${employee.last_name}`}
                  </Link>
                </li>
              )}
            </ul>
          </div>
          :
          null
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Employees);

