import React from 'react';
import { Col, Panel } from 'react-bootstrap';
import { Link } from 'react-router';

function Sidebar() {
  return (
    <Col md={2}>
      <Panel>
        <ul>
          <li><Link to="/departments">Departments</Link></li>
          <li><Link to="/employees">Employees</Link></li>
        </ul>
      </Panel>
    </Col>
  );
}

export default Sidebar;
