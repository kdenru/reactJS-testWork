import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRedirect, IndexRoute, browserHistory } from 'react-router';

import 'bootstrap/dist/css/bootstrap.css';

import App from './containers/App';
import Employees from './containers/Employees';
import Departments from './containers/Departments';
import DepartmentDetails from './containers/Departments/Details';
import EmployeeDetails from './containers/Employees/Details';

import createStore from './redux/configureStore';

const store = createStore();

if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRedirect to="departments" />
        <Route path="departments">
          <IndexRoute component={Departments} />
          <Route path=":id" component={DepartmentDetails} />
        </Route>
        <Route path="employees">
          <IndexRoute component={Employees} />
          <Route path=":id" component={EmployeeDetails} />
        </Route>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
