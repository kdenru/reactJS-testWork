import axios from 'axios';

function getDepartments() {
  return axios.get('http://localhost:3000/departments');
}

function getDepartment(id) {
  return axios.get(`http://localhost:3000/departments/${id}`);
}

function getEmployeesByDepartment(id) {
  return axios.get(`http://localhost:3000/employees?departmentId=${id}`);
}

function updateDepartment(department) {
  return axios.put(`http://localhost:3000/departments/${department.id}`, { name: department.name });
}

function getEmployee(id) {
  return axios.get(`http://localhost:3000/employees/${id}`);
}

function updateEmployee(employee) {
  return axios.put(`http://localhost:3000/employees/${employee.id}`,
    {
      departmentId: employee.departmentId,
      first_name: employee.first_name,
      last_name: employee.last_name
    });
}

export {
  getEmployee,
  getDepartment,
  getDepartments,
  updateEmployee,
  updateDepartment,
  getEmployeesByDepartment,
};
