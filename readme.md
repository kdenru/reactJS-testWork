### setting up

Copy repo to local machine:
```
git clone https://kdenru@gitlab.com/kdenru/reactJS-testWork.git
```
Move to project directory: 
```
cd reactJS-testWork
```
Install dependencies:
```
npm install
```
Run:
```
npm start
```
This command runs json-server at localhost:3000 and webpack-dev-server at localhost:8081.